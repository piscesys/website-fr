<center><font size=7>Un nouveau départ</font size=7></center>

![Screenshot_2023-01-13-08-31-41-0245411524.png](https://gitlab.com/piscesys/website/-/raw/master/source/images/3.png)

## Votre Piscesys préféré est de retour

**L'image d'installation du système sera disponible dans le courant de l'année.**



## Pourquoi choisir Piscesys?

- Piscesys est une distribution Linux axée sur l'utilisateur et centrée sur lui.

## Un nouveau départ pour répondre à l'attente de l'utilisateur

-    Tout le monde l'attend avec impatience et le nouveau Piscesys est destiné à apporter de nouvelles expériences.
-    Grâce à notre enthousiasme pour ce projet , Piscesys ne cessera de s'améliorer.

## Revenir à l'essentiel et continuer à aimer

-    **Le nouveau forum est désormais en ligne.**
-    Bienvenue à notre adresse open source:
-    https://gitlab.com/piscesys/
-    Il est prévu que la nouvelle année vous apporte une réponse satisfaisante.
